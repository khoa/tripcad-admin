ActiveAdmin.register BestTime do
  permit_params :destination_id, :best_time, :best_date_from, :best_date_to, :description
  index do
    id_column
    column :destination do |best_time|
      link_to (best_time.destination.nil? ? '' : best_time.destination.name), admin_destination_path(best_time.destination)
    end
    column :best_time
    column :best_date_from
    column :best_date_to
    column :description
    actions
  end
end
